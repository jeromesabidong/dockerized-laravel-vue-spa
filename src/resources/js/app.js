require('./bootstrap');

import VueRouter from "vue-router";
import Vuex from 'vuex';
import Vue from "vue";
import router from "./routes"
import index from "./Index"
import moment from "moment"
import FatalError from "./shared/components/FatalError"
import StarRating from "./shared/components/StarRating"
import Success from "./shared/components/Success"
import ValidationErrors from "./shared/components/ValidationErrors"
import storeDefinition from "./store"
import axios from "axios";

window.Vue = require('vue');

Vue.use(VueRouter);
Vue.use(Vuex);

Vue.filter("fromNow", value => moment(value).fromNow());

Vue.component("star-rating", StarRating);
Vue.component("fatal-error", FatalError);
Vue.component("validation-errors", ValidationErrors);
Vue.component("success", Success);

const store = new Vuex.Store(storeDefinition);

window.axios.interceptors.response.use(
    response => response,
    error => {
        if (401 === error.response.status) {
            store.dispatch('logout');
        }

        return Promise.reject(error);
    }
);

const app = new Vue({
    el: '#app',
    router: router, // longhand for `router`
    store,
    components: {
        "index": index
    },
    async beforeCreate() {
        this.$store.dispatch('loadStoredState');
        this.$store.dispatch('loadUser');
    }
});
